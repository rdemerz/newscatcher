__version__ = "1.0"

# Retrieve and analyze
# 24/7 streams of news data
import sqlite3

# import requests
import feedparser
import pkg_resources
from tldextract import extract

DB_FILE = pkg_resources.resource_filename("newscatcher", "data/package_rss.db")


def query_db(query: str, fetch_one: bool = True):
    db = sqlite3.connect(DB_FILE, isolation_level=None)
    if fetch_one:
        result = db.execute(query).fetchone()
    else:
        result = db.execute(query).fetchall()
    db.close()
    return result

def clean_url(dirty_url: str) -> str:
    # website.com
    dirty_url = dirty_url.lower()
    o = extract(dirty_url)
    return o.domain + "." + o.suffix


class Newscatcher:
    # search engine
    def __init__(self, website: str, topic: str = None):
        # init with given params
        self.url = clean_url(website.lower())
        self.topic = topic

    def get_feed(self, n: int = None):
        """
        queries the rss server and parses the feed.
        """
        if self.topic:
            sql = f"Select rss_url from rss_main WHERE" \
                  f" clean_url = '{self.url}' AND" \
                  f" topic_unified = '{self.topic}';"
        else:
            sql = f"SELECT rss_url from rss_main WHERE clean_url = '{self.url}' AND main = 1;"

        rss_endpoint = query_db(sql)[0]
        feed = feedparser.parse(rss_endpoint)
        # breakpoint()
        feed["entries"]
        if not feed["entries"]:
            print(
                "\nNo headlines found check internet connection or query parameters\n"
            )
            return
        return feed

    def print_headlines(self, n: int = None) -> None:
        feed = self.get_feed(n)

        headlines = [article["title"] for article in feed["entries"][:n] if "title" in article]

        for idx, headline in enumerate(headlines, start=1):
            print(f"{idx} | {headline}")

    def get_news(self, n: int = None):
        # return results based on current stream
        feed = self.get_feed(n)
        articles = feed["entries"][:n]

        return {
            "url": self.url,
            "topic": self.topic,
            "articles": articles,
        }


def urls(topic=None, language=None, country=None):
    # return urls that matches users parameters
    if language != None:
        language = language.lower()

    if country != None:
        country = country.upper()

    if topic != None:
        topic = topic.lower()

    sql = f"SELECT DISTINCT clean_url from rss_main WHERE topic_unified = '{topic}';"

    ret = query_db(sql, fetch_one=False)
    print(ret)
    if len(ret) == 0:
        print("\nNo websites found for given parameters\n")
        return

    return [x[0] for x in ret]
