from bs4 import BeautifulSoup
from pprint import pprint
from newscatcher import urls, Newscatcher

urls_pol = urls(topic='politics')
print(urls_pol)
for url in urls_pol:
    print(url)
    stories = Newscatcher(url, topic='politics').get_news(n=50)
    print(url,"\n","*"*80)
    for story in stories["articles"]:
        print("=" * 80)
        print(f"   {story['title']}")
        print("=" * 80)
        # print(story)
        try:
            soup = BeautifulSoup(story["content"][0]["value"], 'html.parser')
            pprint(soup.get_text())
        except KeyError:
            pass



# soup = BeautifulSoup(html, 'hmll.parser')
# print(soup.get_text())

